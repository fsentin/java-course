# Java Course @ FER

Java is an *infamous* course on FER lectured by the respectable professor Marko Čupić. 

It is based on lengthy homeworks which require a lot of time and effort. In order for the homeworks to not go to waste I am hereby presenting them on my GitLab account.


Each homework has its own topics. You can see them in the following section. By clicking on each one you can check out the detailed description about the topic of the particular homework or look at its source code.

## Homework topics

  1. [Custom collections & complex numbers](hw01)
  2. [Lexer & Parser](hw02)
  3. [Dictionary, iterable hashtable & vectors](hw03)
  4. [Lsystems & database emulator](hw04)
  5. [Applied cryptography & MyShell](hw05)
  6. [Newton fractals with multithreading](hw06)
  7. [GUI - Chart & Calculator](hw07)
  8. [GUI - Notepad](hw08)
